use serde::{Deserialize, Serialize};
use std::{collections::HashMap, net::SocketAddr};

use crate::InstancePath;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum InstanceManagerRequest {
    InstanceSocketAddr(InstancePath),
    GetRelatedInstances(InstancePath),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum InstanceManagerResponse {
    InstanceSocketAddr(InstancePath, SocketAddr),
    RelatedInstances(HashMap<InstancePath, String>),
}

use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::net::SocketAddr;

/// Messages between an admin console and any manager on the network
#[cfg(any(feature = "management_console", feature = "instance_manager"))]
pub mod console_manager;
/// Messages between an admin console and the manager network
#[cfg(any(feature = "management_console", feature = "instance_manager"))]
pub mod console_network;

/// Messages requesting information from any manager on the network
#[cfg(any(feature = "generic_instance", feature = "instance_manager"))]
pub mod instance_manager;
/// Messages on spawning and dropping instances from any other instance to the manager network
#[cfg(any(feature = "generic_instance", feature = "instance_manager"))]
pub mod instance_network;

/// Messages between managers on the network -- for use in peer discovery, validation, and the
/// transmission of the actual Raft requests and responses
#[cfg(feature = "instance_manager")]
pub mod manager_manager;
/// Messages between a manager and the whole network -- on sharing and updating configuration or
/// updating the physical server's health
#[cfg(feature = "instance_manager")]
pub mod manager_network;

/// The data types in configuring each manager in the network -- used in updating the configuration
/// via a management console
#[cfg(any(feature = "management_console", feature = "instance_manager"))]
pub mod manager_configuration;

/// The message sent when forming a connection with an instance manager simply defining the instance
/// type, and thus what message types are to be used in deserialization. All messages include the
/// SocketAddr that the connection in question listens on such that they can be added to the
/// distributed state
#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub enum Greeting {
    Console(SocketAddr),
    Instance(SocketAddr),
    Peer(SocketAddr),
}

/// A simple struct with statistics on physical server performance to be used in deciding the
/// server to spawn an instance on
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ServerHealth {
    pub ports_free: u8,
    pub cpu_use_as_decimal_fraction: f32,
    pub ram_use_as_decimal_fraction: f32,
    pub ram_free_in_mb: u32,
}

/// A generic message type that contains a payload of the resquests and responses defined in this
/// module's submodules
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct InstanceManagerMessage<T> {
    pub id: u64,
    pub payload: T,
}

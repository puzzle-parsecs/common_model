use super::manager_configuration::InstanceConfig;

use serde::{Deserialize, Serialize};

use crate::{
    instance_management::{manager_configuration, ServerHealth},
    InstancePath,
};

// TODO: Explicit queries

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum ConsoleManagerRequest {
    GetCoreConfig(manager_configuration::CoreConfig),
    GetInstanceConfig(String),
    // QueryConfig(Query),
    // QueryState(Query),
    GetServerHealth(u64),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum ConsoleManagerResponse {
    CoreConfig(manager_configuration::CoreConfig),
    InstanceConfig(InstanceConfig),
    // ConfigQueryResult(QueryResult),
    // StateQueryResult(QueryResult),
    ServerHealth(ServerHealth),
}

use serde::{Deserialize, Serialize};

#[cfg(feature = "instance_management")]
pub mod instance_management;

/// A type used to identify a specific instance in the tree of instances, from most generic to most
/// specific -- eg [system_id, combat_id, ship_id, puzzle_id]
pub type InstancePath = uuid::Uuid;
